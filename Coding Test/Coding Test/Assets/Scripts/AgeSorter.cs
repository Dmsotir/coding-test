﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AgeSorter : MonoBehaviour
{
	public Text yearsOutputText;
	public Text numeberAliveOutputText;
	public List<string> sortedYears;//list that can be viewed from the inspector with the years and their corresponding number alive

	private List<Vector2> mostAliveYears;//Vector2 x = year of most alive, y = amount of people alive
	private List<int> ageListBorn;//list of the years people were born
	private List<int> ageListDeath;//list of the years people died
	private int peopleAlive;//total people alive in a given year

	public void Sort()
	{
		if (Resources.Load("AgeList") != null)//checking if the file is available
		{
			//initializing
			sortedYears = new List<string>();
			ageListBorn = new List<int>();
			ageListDeath = new List<int>();
			mostAliveYears = new List<Vector2>();
			peopleAlive = 0;


			TextAsset ages = Resources.Load("AgeList") as TextAsset;//loading file created in other script
			//TextAsset ages = Resources.Load("AgeList500k") as TextAsset;
			string[] agesListString = ages.text.Split('|');//spliting lives into seperate strings

			if (agesListString.Length <= 0 || (agesListString.Length == 1 && agesListString[0] == ""))
				return;//returns if the data file was empty or was null

			//adding the birth and death years to seperate lists for easier sorting
			foreach (string life in agesListString)
			{
				string[] temp = life.Split('-');
				ageListBorn.Add(int.Parse(temp[0]));
				ageListDeath.Add(int.Parse(temp[1]));
			}

			ageListBorn.Sort();//sorting
			ageListDeath.Sort();//sorting


			for (int year = 1900; year < 2001; year++)//parses each year to count the people alive
			{
				foreach (int born in ageListBorn)//if someone is born, increment
					if (born == year)
						peopleAlive++;

				foreach (int death in ageListDeath)//if someone died during the last year, decrement
					if (death == year - 1)//if it is not inclusive on death, remove the '-1'
						peopleAlive--;

				if (mostAliveYears.Count < 1)//if no current year set as most
					mostAliveYears.Add(new Vector2(year, peopleAlive));
				else if (peopleAlive == mostAliveYears[0].y)//if current year is tied with previous year
					mostAliveYears.Add(new Vector2(year, peopleAlive));
				else if (peopleAlive > mostAliveYears[0].y)//if current year is greater than last year
				{
					mostAliveYears = new List<Vector2>();
					mostAliveYears.Add(new Vector2(year, peopleAlive));
				}

			//*******Sorted view of the list for viewing in the inspector**********
				sortedYears.Add(year + " - " + peopleAlive + " Alive");
			//**************************************************************************
			}

			sortedYears.Sort();//sorting the inspector list by year

			Debug.Log("Sorted");

			yearsOutputText.text = null;

			for (int i = 0; i < mostAliveYears.Count; i++)//parses the mostAliveYears list and prints the year to the game view.
				yearsOutputText.text += mostAliveYears[i].x.ToString() + "  ";

			numeberAliveOutputText.text = "with " + mostAliveYears[0].y.ToString() + " alive.";//prints amount of people alive in the largest year to the game view.
		}
	}
}