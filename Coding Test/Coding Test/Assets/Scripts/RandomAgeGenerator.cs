﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class RandomAgeGenerator : MonoBehaviour
{
	[Header("Set this to the number of people in the dataset.")]
	public int numberOfPeople;//number of people to use in the dataset

	private string years;//string that will contain all of the years of the lives
	private int birthYear;
	private int deathYear;

	public void GenerateYears ()
	{
		years = "";//empties the years string before starting

		for (int i=0; i<numberOfPeople; i++)//loops X amount of times creating a birth and death year each loop
		{
			birthYear = UnityEngine.Random.Range(1900, 2001);//random birth year between 1900 & 2000
			deathYear = UnityEngine.Random.Range(birthYear, 2001);//random death year between birthYear & 2000

			if (i < numberOfPeople - 1)//if not the last person, add a delimiter
				years += (birthYear.ToString() + "-" + deathYear.ToString() + "|");
			else if (i == numberOfPeople - 1)//if last person, dont add delimiter
				years += (birthYear.ToString() + "-" + deathYear.ToString());
		}

		WriteToFile();//writes the file to Resources folder in /Assets.
	}

	void WriteToFile()
	{
		if (years.Length > 0 && years != null)//makes sure theres at least 1 entry
		{
			System.IO.File.WriteAllText(Application.dataPath + "/Resources/AgeList.txt", years);
			Debug.Log("File Written to the Resources Folder located at " + Application.dataPath + "/Resources/AgeList.txt");
		}
	}
}