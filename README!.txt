When you open the Unity project, navigate to the 'Scenes' folder and open the 'Output' scene.

The 'Scene' GameObject houses two scripts. On RandomAgeGenerator you can set the number of people
you want to use in the dataset and it will randomly generate .txt file in the 'Resources' folder when you
click the button during runtime. **Warning** It will overwrite any existing file titled 'AgeList' in that
folder.

The Age Sorter script has a public list that shows each year, 1900-2000, and the number of people
alive during each year. It also displays the text to the Game view after clicking sort.

The dataset used in the example is the one labled AgeList in the 'Resources' folder. The other .txt file
(AgeList500k), has 500,000 people in its dataset. As Unity can take a while(or even crash) running a loop
that large, I have created that one for you. Either rename that file to "AgeList" or comment out line 29 
of AgeSorter and uncomment line 30.

I have made the list inclusive of death year, so if someone dies in a year, they are not taken out of 
having lived in a year until the following year.

If you have any questions about how I did anything, please ask.

Thanks for your time!

David Sotir
DavidSotir@Outlook.com
(815) 679-7886